(c-add-style
 "efl"
 '("gnu"
   (show-trailing-whitespace t)
   (indent-tabs-mode . nil)
   (tab-width . 8)
   (c-offsets-alist .
		    ((defun-block-intro . 3)
		     (statement-block-intro . 3)
		     (case-label . 1)
		     (statement-case-intro . 3)
		     (inclass . 3)
))))

(defun efl-c-mode-hooks ()
     (let ((path (buffer-file-name)))
     (cond
      ((string-match "/efl/src/" path) (c-set-style "efl"))
)))

(add-hook 'c-mode-common-hook 'efl-c-mode-hooks)

(provide 'efl-mode)
